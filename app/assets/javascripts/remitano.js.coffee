@remitano = angular.module('remitano',["ngResource"])

@remitano.config ["$httpProvider", ($httpProvider) ->
  $httpProvider.defaults.headers.common.Accept = 'application/json'
  $httpProvider.defaults.transformResponse.push (data) ->
    data = null if data == "null"
    data
]


