remitano.controller "HomeCtrl", ["$http", "$scope", ($http, $scope) ->
  $scope.buyOrder = {}
  $scope.sellOrder = {}

  fetchOrders = ->
    $http(
      url: "/orders"
      method: "GET"
    ).success((response) ->
      $scope.sell_orders = response.sell_orders
      $scope.buy_orders = response.buy_orders
    ).error((response) ->
      alert("error")
    )

  $scope.createOrder = (side) ->
    order = if side == "buy" then $scope.buyOrder else $scope.sellOrder
    order.side = side
    $http(
      url: "/orders"
      method: "POST"
      data:
        order: order
    ).success((response) ->
      $scope.clearForm(side)
      fetchOrders()
    ).error((response) ->
      alert("error")
    )

  $scope.clearForm = (side) ->
    order = if side == "buy" then $scope.buyOrder else $scope.sellOrder
    order = {}

  fetchOrders()
]

