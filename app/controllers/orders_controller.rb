class OrdersController < ApplicationController
  respond_to :json

  def index
    @buy_orders = Order.find(side: "buy")
    @sell_orders = Order.find(side: "sell")
  end

  def create
    Order.create(order_params)
    render json: { success: true }
  end

  private
    def order_params
      params.require(:order).permit(:side, :quantity, :price, :order_type).tap do |p|
        p[:price] = p[:price].try(:to_d)
        p[:quantity] = p[:quantity].to_d
      end
    end
end
