class SiteConfig
  DEFAULTS = {
    app_name: "Remitano",
    domain: "http://remitano.com",
  }.with_indifferent_access

  def self.app_name_no_space
    self.app_name.gsub(/ /, "")
  end

  def self.domain_no_protocol
    Rails.cache.fetch("domain_no_protocol") do
      self.domain.sub(/^https?\:\/\//, '').sub(/^www./,'')
    end
  end

  def self.cap_domain_no_protocol
    Rails.cache.fetch("cap_domain_no_protocol") do
      self.cap_domain.sub(/^https?\:\/\//, '').sub(/^www./,'')
    end
  end

  def self.method_missing(method, *args)
    method_name = method.to_s
    if method_name =~ /=$/
      var_name = method_name.gsub('=', '')
      value = args.first
      SiteConfig::DEFAULTS[var_name] = value
    else
      SiteConfig::DEFAULTS[method_name]
    end
  end

  def self.app_id
    SiteConfig.app_name.underscore
  end
end

