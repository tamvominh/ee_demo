class Order < Ohm::Model
  include     Ohm::Timestamps
  include     Ohm::DataTypes

  attribute :order_type
  attribute :quantity, Type::Decimal
  attribute :currency
  attribute :side
  attribute :filled_quantity, Type::Decimal
  attribute :status
  attribute :price, Type::Decimal
  attribute :user_id, Type::Integer

  index :currency
  index :status
  index :user_id
  index :side

  SIDES = { buy: 'buy', sell: 'sell' }
  STATUSES = { live: 'live', cancelled: 'cancelled' }
end
